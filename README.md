# bostrom_deconvolution_project

Deconvolution scripts for "RNA‐seq Analysis of Peri‐Implant Tissue Shows Differences in Immune, Notch, Wnt, and Angiogenesis Pathways in Aged Versus Young Mice"

## Scripts include two scripts:

bostrom_dat_out.R: scripts used to generate the files from a cell type database. Some functions included at the bottom are older versions of functions going to be added to PanglaoSCDC. Conditions is 100 iterations of 100 randomly sampled cells of 27 cell types selected. ggradar plot generated via script below

old_funcs_PanglaoSCDC.R old and now defunct functions from WIP project PanglaoSCDC which uses PanglaoDB to randomly sample for cells for a cell type and bootstrap test these references generated through SCDC to define cell type proportions in a bulk RNAseq sample.

## data input:

bostrom_props_100iters_100depth.rds: full scdc output when running scdc, including basis matrices and cell type deconvolution results, final output used to generate ggradar

Randomly sampled iterations zip link: https://drive.google.com/drive/folders/1fWAhoBmChen_czH19JhJkKi_vHQmM28P?usp=sharing
- sm is the sparse matrix of the 100 iterations of the 37 cell types
- pheno is the phenotypical information of the cells stored in each of the 100 iterations

For more information on how these iterations were generated please see PanglaoSCDC.